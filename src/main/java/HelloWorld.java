import static spark.Spark.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class HelloWorld {
    private static final Logger logger = LogManager.getLogger(HelloWorld.class);
    
    public static void main(String[] args) {
        logger.warn("Testing logging -----");
        get("/hello", (req, res) -> "Hello World");
    }
}